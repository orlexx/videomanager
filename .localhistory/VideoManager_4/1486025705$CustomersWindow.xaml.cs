﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace VideoManager_4
{
    /// <summary>
    /// Interaction logic for CustomersWindow.xaml
    /// </summary>
    public partial class CustomersWindow : Window
    {
        string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        SqlDataAdapter adapter;
        DataTable customersTable;
        bool vidsShown;
        public CustomersWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string sqlEx = "SELECT * FROM T_Customers";
            customersTable = new DataTable();
            SqlConnection connection = null;

            connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sqlEx, connection);
            adapter = new SqlDataAdapter(command);
            connection.Open();
            adapter.Fill(customersTable);
            customersGrid.ItemsSource = customersTable.DefaultView;
            vidsShown = true;
            


            if (connection != null)
                connection.Close();

        }
    }
}
