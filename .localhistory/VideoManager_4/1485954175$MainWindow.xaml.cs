﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace VideoManager_4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        SqlDataAdapter adapter;
        DataTable studentsTable;
        public MainWindow()
        {
            InitializeComponent();
            
            
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string sqlEx = "SELECT * FROM tblStudents";
            SqlConnection connection = null;
            
                connection = new SqlConnection(connectionString);
                SqlCommand command = new SqlCommand(sqlEx, connection);
                adapter = new SqlDataAdapter(command);
                connection.Open();
                adapter.Fill(studentsTable);
                studentsGrid.ItemsSource = studentsTable.DefaultView;

            
                if (connection != null)
                    connection.Close();
            
        }
    }
}
