﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace VideoManager_4
{
    /// <summary>
    /// Interaction logic for CustomerInfoWindow.xaml
    /// </summary>
    public partial class CustomerInfoWindow : Window
    {
        string sqlQuery;
        DataSet ds;
        public int id { get; set; }
        public CustomerInfoWindow(int id)
        {
            InitializeComponent();
            this.id = id;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
            ID_TextBlock.Text = id.ToString();
            string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            SqlConnection con = new SqlConnection(cs);
            sqlQuery = "SELECT * FROM T_Customers WHERE p_customer_nr = " + ID_TextBlock.Text;
            SqlDataAdapter da = new SqlDataAdapter(sqlQuery, con);
            ds = new DataSet();
            da.Fill(ds, "Customers");
            
            DataRow dr = ds.Tables["Customers"].Rows[0];
            Title_ComboBox.SelectedValue = dr["title"].ToString();
            FirstName_TextBox.Text = dr["firstName"].ToString();
            LastName_TextBox.Text = dr["lastName"].ToString();
            Street_TextBox.Text = dr["street"].ToString();
            PostalCode_TextBox.Text = dr["postalCode"].ToString();
            City_TextBox.Text = dr["city"].ToString();
            DateTime bd = (DateTime)dr["birthdate"];
            BirthDate_TextBox.Text = bd.ToShortDateString();            
        }

        private void Title_ComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            List<string> data = new List<string>();
            data.Add("Mr");
            data.Add("Ms");
            data.Add("Mrs");

            // ... Get the ComboBox reference.
            var comboBox = sender as ComboBox;

             //... Assign the ItemsSource to the List.
            comboBox.ItemsSource = data;
            
        }

        private void Save_Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult res = MessageBox.Show("Do you want to save the changes?", 
                "Question", MessageBoxButton.YesNo, 
                MessageBoxImage.Question, MessageBoxResult.No);
            switch(res)
            {
                case MessageBoxResult.Yes:
                    SaveChanges();
                    break;
                case MessageBoxResult.No:
                    break;
            }
        }
        private void SaveChanges()
        {
            string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            SqlConnection con = new SqlConnection(cs);

            string title = Title_ComboBox.SelectedValue.ToString();
            string firstName = FirstName_TextBox.Text;
            string lastName = LastName_TextBox.Text;
            string street = Street_TextBox.Text;
            int postalCode = Convert.ToInt32(PostalCode_TextBox.Text);
            string city = City_TextBox.Text;
            DateTime birthdate = DateTime.Parse(BirthDate_TextBox.Text);
            string sqlEx = "sp_UpdateCustomer";
            string id = ID_TextBlock.Text;

            con.Open();
            SqlCommand command = new SqlCommand(sqlEx, con);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter titleParam = new SqlParameter
            {
                ParameterName = "@title",
                Value = title
            };
            command.Parameters.Add(titleParam);

            SqlParameter idParam = new SqlParameter
            {
                ParameterName = "@p_customer_nr",
                Value = id
            };
            command.Parameters.Add(idParam);

            SqlParameter firstNameParam = new SqlParameter
            {
                ParameterName = "@firstName",
                Value = firstName
            };
            command.Parameters.Add(firstNameParam);

            SqlParameter lastNameParam = new SqlParameter
            {
                ParameterName = "@lastName",
                Value = lastName
            };
            command.Parameters.Add(lastNameParam);

            SqlParameter streetParam = new SqlParameter
            {
                ParameterName = "@street",
                Value = street
            };
            command.Parameters.Add(streetParam);

            SqlParameter postalCodeParam = new SqlParameter
            {
                ParameterName = "@postalCode",
                Value = postalCode
            };
            command.Parameters.Add(postalCodeParam);

            SqlParameter cityParam = new SqlParameter
            {
                ParameterName = "@city",
                Value = city
            };
            command.Parameters.Add(cityParam);

            SqlParameter birthdateParam = new SqlParameter
            {
                ParameterName = "@birthdate",
                Value = birthdate
            };
            command.Parameters.Add(birthdateParam);

            var result = command.ExecuteNonQuery();
            Message_TextBlock.Text = "Updated.";
        }
        private void Cancel_Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Delete_Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult res = MessageBox.Show("Do you want to delete this customer?",
               "Question", MessageBoxButton.YesNo,
               MessageBoxImage.Question, MessageBoxResult.No);
            switch (res)
            {
                case MessageBoxResult.Yes:
                    DeleteUser();
                    break;
                case MessageBoxResult.No:
                    break;
            }
        }

        private void DeleteUser()
        {
            string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using(SqlConnection connection = new SqlConnection(cs))
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.CommandText = "DELETE FROM T_Customers WHERE p_customer_nr = " + id;
                command.Connection = connection;
                command.ExecuteNonQuery();
            }
            ID_TextBlock.Foreground = Brushes.Gray;
            Title_ComboBox.IsEnabled = false;
            FirstName_TextBox.IsEnabled = false;
            LastName_TextBox.IsEnabled = false;
            Street_TextBox.IsEnabled = false;
            PostalCode_TextBox.IsEnabled = false;
            City_TextBox.IsEnabled = false;
            BirthDate_TextBox.IsEnabled = false;
            CustomerId_TextBlock.IsEnabled = false;
            Message_TextBlock.Text = "Customer deleted.";
            Title_ComboBox.IsEnabled = false;
            
            //Close();
        }

        private void ChangeImg_Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
