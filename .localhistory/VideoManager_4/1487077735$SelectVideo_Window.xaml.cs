﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace VideoManager_4
{
    /// <summary>
    /// Interaction logic for SelectVideo_Window.xaml
    /// </summary>
    public partial class SelectVideo_Window : Window
    {
        string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        SqlDataAdapter adapter;
        DataTable videoTable;
        DataTable customersTable;
        SqlConnection connection;
        SqlCommand command;
        public int customerId { get; set; }
        public SelectVideo_Window(int id)
        {
            InitializeComponent();
            customerId = id;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadVideos();
        }
        private void videoGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BorrowButton.IsEnabled = true;
            InfoButton.IsEnabled = true;
        }
        private void LoadVideos()
        {
            BorrowButton.IsEnabled = false;
            InfoButton.IsEnabled = false;
            string sqlEx = "SELECT * FROM T_Videos";
            videoTable = new DataTable();
            connection = new SqlConnection(connectionString);
            command = new SqlCommand(sqlEx, connection);
            adapter = new SqlDataAdapter(command);
            connection.Open();
            adapter.Fill(videoTable);
            videoGrid.ItemsSource = videoTable.DefaultView;

            if (connection != null)
                connection.Close();
        }

        private void InfoButton_Click(object sender, RoutedEventArgs e)
        {
            string idString = ((DataRowView)(videoGrid.SelectedItem)).Row.ItemArray[0].ToString();
            int id = Convert.ToInt32(idString);
            VideoInfo_Window viw = new VideoInfo_Window(id);
            viw.Show();
        }
    }
}
