﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using Microsoft.Win32;

namespace VideoManager_4
{
    /// <summary>
    /// Interaction logic for CustomersWindow.xaml
    /// </summary>
    public partial class CustomersWindow : Window
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        SqlDataAdapter adapter;
        SqlConnection connection = new SqlConnection(connectionString);
        DataTable customersTable;
        public int videoId { get; set; }
        public int customerId { get; set; }

        public CustomersWindow()
        {
            InitializeComponent();
        }

        public CustomersWindow(int videoId)
        {
            InitializeComponent();
            this.videoId = videoId;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string sqlEx = "SELECT * FROM T_Customers";
            customersTable = new DataTable();
            
            SqlCommand command = new SqlCommand(sqlEx, connection);
            adapter = new SqlDataAdapter(command);
            connection.Open();
            adapter.Fill(customersTable);
            customersGrid.ItemsSource = customersTable.DefaultView;
                 
            if (connection != null)
                connection.Close();

        }

        private void findButton_Click(object sender, RoutedEventArgs e)
        {
            string sqlEx = "SELECT * FROM T_Customers WHERE firstName LIKE '%' + @name + '%' OR lastName LIKE '%' + @name + '%'";
            string name = findTextBox.Text;
            customersTable = new DataTable();
            
            SqlCommand command = new SqlCommand(sqlEx, connection);
            SqlParameter nameParam = new SqlParameter
            {
                ParameterName = "@name",
                Value = name
            };
            command.Parameters.Add(nameParam);
            adapter = new SqlDataAdapter(command);
            connection.Open();
            adapter.Fill(customersTable);
            customersGrid.ItemsSource = customersTable.DefaultView;

            if (connection != null)
                connection.Close();
        }

        private void infoButton_Click(object sender, RoutedEventArgs e)
        {            
            string idString = ((DataRowView)(customersGrid.SelectedItem)).Row.ItemArray[0].ToString();
            int id = Convert.ToInt32(idString);
            CustomerInfoWindow ciw = new CustomerInfoWindow(id);
            ciw.Show();
        }

        private void customersGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            infoButton.IsEnabled = true;
            if (customersGrid.SelectedItem != null)
            {
                string idString = ((DataRowView)(customersGrid.SelectedItem)).Row.ItemArray[0].ToString();
                customerId = Convert.ToInt32(idString);
            }
        }

        private void AddNew_Button_Click(object sender, RoutedEventArgs e)
        {
            AddNewUser_Window anrWindor = new AddNewUser_Window();
            anrWindor.Show();
        }

        private void borrow_Button_Click(object sender, RoutedEventArgs e)
        {

            int borrowedVideos;
            string sqlQuery2 = "SELECT COUNT(fp_customer_nr) FROM T_Customers_Videos WHERE fp_customer_nr = " + customerId;

            connection.Open();
            //if (videoGrid.SelectedItem != null)                                             //Sieht sinnlos aus, aber anders geht nicht.
            //{
            //    SqlCommand sqlcmd2 = new SqlCommand(sqlQuery2, connection);
            //    Int32.TryParse(sqlcmd2.ExecuteScalar().ToString(), out borrowedVideos);
            //}
            //else
            //{
                SqlCommand sqlcmd2 = new SqlCommand(sqlQuery2, connection);
                Int32.TryParse(sqlcmd2.ExecuteScalar().ToString(), out borrowedVideos);
            //}
            connection.Close();


            if (borrowedVideos < 5)
            {
                SelectVideo_Window svw = new SelectVideo_Window(customerId);
                svw.Show();
            }
            else MessageBox.Show("Sorry, you cannot borrow more than five videos at the same time.");
            
        }
    }
}

