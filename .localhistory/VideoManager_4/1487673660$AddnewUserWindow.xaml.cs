﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using Microsoft.Win32;

namespace VideoManager_4
{
    /// <summary>
    /// Interaction logic for AddnewUserWindow.xaml
    /// </summary>
    public partial class AddnewUserWindow : Window
    {
        static string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        SqlConnection connection = new SqlConnection(cs);
        SqlCommand sqlcmd = new SqlCommand();
        string picPath;
        public AddnewUserWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void ChangeImg_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog ofdImage = new OpenFileDialog();
                ofdImage.Filter = "JPEG Files|*.jpg|Bitmap Files|*.bmp|Gif Files|*.gif";
                ofdImage.DefaultExt = "jpg";
                ofdImage.Title = "Select picture";
                ofdImage.FilterIndex = 1;
                if (ofdImage.ShowDialog() == true)
                {
                    picPath = ofdImage.FileName.ToString();

                    //Stream s = ofdImage.OpenFile();
                    Customer_Image.Source = new BitmapImage(new Uri(ofdImage.FileName));
                    //picPath = ofdImage.SafeFileName.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Cancel_Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Save_Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult res = MessageBox.Show("Do you want to save the changes?",
                "Question", MessageBoxButton.YesNo,
                MessageBoxImage.Question, MessageBoxResult.No);
            switch (res)
            {
                case MessageBoxResult.Yes:
                    SaveChanges();
                    break;
                case MessageBoxResult.No:
                    break;
            }
        }
        private void SaveChanges()
        {
            string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            SqlConnection con = new SqlConnection(cs);

            string firstName = FirstName_TextBox.Text;
            string lastName = LastName_TextBox.Text;
            string title = Title_ComboBox.SelectedValue.ToString();
            string street = Street_TextBox.Text;
            string postalCode = PostalCode_TextBox.Text;
            string city = City_TextBox.Text;
            DateTime birthDate = Convert.ToDateTime(BirthDate_TextBox.Text);

            MemoryStream ms = new MemoryStream();
            byte[] img = null;
            if (Customer_Image.Source.ToString() != "Assets\\no_pic.jpg"
                && picPath != null)
            {
                FileStream fs = new FileStream(picPath, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                img = br.ReadBytes((int)fs.Length);
            }
            
            string sqlEx = "sp_InsertCustomer";
            con.Open();
            SqlCommand command = new SqlCommand(sqlEx, con);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter firstNameParam = new SqlParameter
            {
                ParameterName = "@firstName",
                Value = firstName
            };
            command.Parameters.Add(firstNameParam);

            SqlParameter lastNameParam = new SqlParameter
            {
                ParameterName = "@lastName",
                Value = lastName
            };
            command.Parameters.Add(lastNameParam);

            SqlParameter titleParam = new SqlParameter
            {
                ParameterName = "@title",
                Value = title
            };
            command.Parameters.Add(titleParam);

            SqlParameter streetParam = new SqlParameter
            {
                ParameterName = "@street",
                Value = street
            };
            command.Parameters.Add(streetParam);

            SqlParameter postalCodeParam = new SqlParameter
            {
                ParameterName = "@postalCode",
                Value = postalCode
            };
            command.Parameters.Add(postalCodeParam);

            SqlParameter cityParam = new SqlParameter
            {
                ParameterName = "@city",
                Value = city
            };
            command.Parameters.Add(cityParam);

            SqlParameter birthdateParam = new SqlParameter
            {
                ParameterName = "@birthdate",
                Value = birthDate
            };
            command.Parameters.Add(birthdateParam);

            SqlParameter posterParam = new SqlParameter
            {
                ParameterName = "@customerImage",
                Value = DBNull.Value
            };
            command.Parameters.Add(posterParam);
            if (Customer_Image.Source.ToString() != "Assets\\no_pic.jpg")
                posterParam.Value = img;

            SqlParameter idParam = new SqlParameter
            {
                ParameterName = "@p_customer_nr",
                
                SqlDbType = SqlDbType.Int,
                Direction = ParameterDirection.Output
            };
            command.Parameters.Add(idParam);

            var result = command.ExecuteNonQuery();
            MessageBox.Show("A new customer (" + firstName + " " + 
                lastName +
                ") has been added");
            ID_TextBlock.Text = idParam.Value.ToString();
        }

        private void Title_ComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            List<string> data = new List<string>();
            data.Add("Mr");
            data.Add("Mrs");
            data.Add("Ms");
            
            var comboBox = sender as ComboBox;
            comboBox.ItemsSource = data;
        }

        private void Image_Loaded(object sender, RoutedEventArgs e)
        {

        }
        public byte[] getJPGFromImageControl(BitmapImage imageC)
        {
            MemoryStream memStream = new MemoryStream();
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(imageC));
            encoder.Save(memStream);
            return memStream.ToArray();
        }
    }
}
