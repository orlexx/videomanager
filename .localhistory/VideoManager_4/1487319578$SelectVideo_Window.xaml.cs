﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace VideoManager_4
{
    /// <summary>
    /// Interaction logic for SelectVideo_Window.xaml
    /// </summary>
    public partial class SelectVideo_Window : Window
    {
        string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        SqlDataAdapter adapter;
        DataTable videoTable;
        SqlConnection connection;
        SqlCommand command;
        public int customerId { get; set; }
        public int videoId { get; set; }
        string idString;

        public SelectVideo_Window(int customerid)
        {
            InitializeComponent();
            this.customerId = customerid;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadVideos();
        }
        private void videoGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BorrowButton.IsEnabled = true;
            InfoButton.IsEnabled = true;
            idString = ((DataRowView)(videoGrid.SelectedItem)).Row.ItemArray[0].ToString();
            videoId = Convert.ToInt32(idString);
        }
        private void LoadVideos()
        {
            BorrowButton.IsEnabled = false;
            InfoButton.IsEnabled = false;
            string sqlEx = "SELECT * FROM T_Videos";
            videoTable = new DataTable();
            connection = new SqlConnection(connectionString);
            command = new SqlCommand(sqlEx, connection);
            adapter = new SqlDataAdapter(command);
            connection.Open();
            adapter.Fill(videoTable);
            videoGrid.ItemsSource = videoTable.DefaultView;

            if (connection != null)
                connection.Close();
        }

        private void InfoButton_Click(object sender, RoutedEventArgs e)
        {       
           // string idString = ((DataRowView)(videoGrid.SelectedItem)).Row.ItemArray[0].ToString();
           //videoId = Convert.ToInt32(idString);
            VideoInfo_Window viw = new VideoInfo_Window(videoId);
            viw.Show();
        }

        private void BorrowButton_Click(object sender, RoutedEventArgs e)
        {
            int availableAmount;
            string sqlQuery1 = "SELECT amountAvailable FROM T_Videos WHERE " +
                "p_vid_nr = " + videoId.ToString();
            string sqlQuery2 = "SELECT COUNT (*) FROM T_Customers_Videos WHERE fp_customer_nr = "
                + customerId + " AND fp_vid_nr = " + videoId;

            connection = new SqlConnection(connectionString);
            SqlCommand sqlcmd1 = new SqlCommand(sqlQuery1, connection);
            connection.Open();
            Int32.TryParse(sqlcmd1.ExecuteScalar().ToString(), out availableAmount);
                     
                if (availableAmount > 0)
                {
                    string sqlEx = "sp_InsertCustomer_Video";

                    SqlCommand sqlcmd = new SqlCommand(sqlEx, connection);
                    sqlcmd.CommandType = CommandType.StoredProcedure;

                    string borrowDate = DateTime.Now.ToShortDateString();

                    SqlParameter customerIdParam = new SqlParameter
                    {
                        ParameterName = "@fp_customer_nr",
                        Value = customerId
                    };
                    sqlcmd.Parameters.Add(customerIdParam);

                    SqlParameter videoIdParam = new SqlParameter
                    {
                        ParameterName = "@fp_vid_nr",
                        Value = idString
                    };
                    sqlcmd.Parameters.Add(videoIdParam);

                    SqlParameter borrowDateParam = new SqlParameter
                    {
                        ParameterName = "@borrowDate",
                        Value = DateTime.Now
                    };
                    sqlcmd.Parameters.Add(borrowDateParam);

                    SqlParameter returnDateParam = new SqlParameter
                    {
                        ParameterName = "@returnDate",
                        Value = DateTime.Now.AddDays(5)
                    };
                    sqlcmd.Parameters.Add(returnDateParam);

                    //sqlcmd1.CommandText = sqlQuery1;
                    using (connection)
                    {
                        sqlcmd.ExecuteNonQuery();
                        availableAmount--;
                        sqlQuery1 = "UPDATE T_Videos SET amountAvailable = " + availableAmount +
                                "WHERE p_vid_nr = " + videoId;
                        sqlcmd1 = new SqlCommand(sqlQuery1, connection);
                        sqlcmd1.ExecuteNonQuery();
                    }



                    MessageBox.Show("Video borrowed successfully. Has to be returned on " + DateTime.Now.AddDays(5).ToShortDateString());

                    Close();
                }
                else MessageBox.Show("Sorry, this video is not available.");
           

        }
    }
}
