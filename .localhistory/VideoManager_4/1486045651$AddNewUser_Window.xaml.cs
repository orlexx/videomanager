﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace VideoManager_4
{
    /// <summary>
    /// Interaction logic for AddNewUser_Window.xaml
    /// </summary>
    public partial class AddNewUser_Window : Window
    {
        public AddNewUser_Window()
        {
            InitializeComponent();
        }

        private void Title_ComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            List<string> data = new List<string>();
            data.Add("Mr");
            data.Add("Ms");
            data.Add("Mrs");
            // ... Get the ComboBox reference.
            var comboBox = sender as ComboBox;

            // ... Assign the ItemsSource to the List.
            comboBox.ItemsSource = data;

            // ... Make the first item selected.
            comboBox.SelectedIndex = 0;
        }

        private void Save_Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult res = MessageBox.Show("Do you want to save the changes?",
                "Question", MessageBoxButton.YesNo,
                MessageBoxImage.Question, MessageBoxResult.No);
            switch (res)
            {
                case MessageBoxResult.Yes:
                    if (FirstName_TextBox.Text != null &&
                        LastName_TextBox.Text != null &&
                        Street_TextBox.Text != null &&
                        PostalCode_TextBox.Text != null &&
                        City_TextBox.Text != null &&
                        BirthDate_TextBox.Text != null)
                        SaveChanges();
                    else
                        MessageBox.Show("Please type in the customer data completely.");
                    break;
                case MessageBoxResult.No:
                    break;
            }
        }
        private void SaveChanges()
        {
            string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            SqlConnection con = new SqlConnection(cs);

            string title = Title_ComboBox.SelectedValue.ToString();
            string firstName = FirstName_TextBox.Text;
            string lastName = LastName_TextBox.Text;
            string street = Street_TextBox.Text;
            int postalCode = Convert.ToInt32(PostalCode_TextBox.Text);
            string city = City_TextBox.Text;
            DateTime birthdate = DateTime.Parse(BirthDate_TextBox.Text);
            string sqlEx = "sp_InsertCustomer";
            string id = ID_TextBlock.Text;

            con.Open();
            SqlCommand command = new SqlCommand(sqlEx, con);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter titleParam = new SqlParameter
            {
                ParameterName = "@title",
                Value = title
            };
            command.Parameters.Add(titleParam);

            SqlParameter idParam = new SqlParameter
            {
                ParameterName = "@p_customer_nr",
                Value = id
            };
            command.Parameters.Add(idParam);

            SqlParameter firstNameParam = new SqlParameter
            {
                ParameterName = "@firstName",
                Value = firstName
            };
            command.Parameters.Add(firstNameParam);

            SqlParameter lastNameParam = new SqlParameter
            {
                ParameterName = "@lastName",
                Value = lastName
            };
            command.Parameters.Add(lastNameParam);

            SqlParameter streetParam = new SqlParameter
            {
                ParameterName = "@street",
                Value = street
            };
            command.Parameters.Add(streetParam);

            SqlParameter postalCodeParam = new SqlParameter
            {
                ParameterName = "@postalCode",
                Value = postalCode
            };
            command.Parameters.Add(postalCodeParam);

            SqlParameter cityParam = new SqlParameter
            {
                ParameterName = "@city",
                Value = city
            };
            command.Parameters.Add(cityParam);

            SqlParameter birthdateParam = new SqlParameter
            {
                ParameterName = "@birthdate",
                Value = birthdate
            };
            command.Parameters.Add(birthdateParam);

            var result = command.ExecuteScalar();
            object customerID = result;
            ID_TextBlock.Text = customerID.ToString();
            Message_TextBlock.Text = "New customer has been added.";
        }

        private void Cancel_Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
