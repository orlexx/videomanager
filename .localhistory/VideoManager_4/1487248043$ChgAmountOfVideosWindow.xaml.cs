﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace VideoManager_4
{
    /// <summary>
    /// Interaction logic for ChgAmountOfVideosWindow.xaml
    /// </summary>
    public partial class ChgAmountOfVideosWindow : Window
    {
        int originalAmount;
        int newAmount;
        int delta;
        static string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        SqlConnection con = new SqlConnection(cs);
        string sqlQuery;
        int id;

        public ChgAmountOfVideosWindow(int id)
        {
            InitializeComponent();
            this.id = id;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            sqlQuery = "SELECT amountTotal FROM T_Videos WHERE " +
                "p_vid_nr = " + id;
            SqlCommand sqlcmd = new SqlCommand(sqlQuery, con);
            con.Open();

            Int32.TryParse(sqlcmd.ExecuteScalar().ToString(), out originalAmount);

            //SqlDataReader reader = sqlcmd.ExecuteReader();
            //while (reader.Read())
            //{
            //    string origAmountString = reader.GetString(0);
            //    originalAmount = Convert.ToInt32(origAmountString);
            //}
        }

        private void IncreaseButton_Click(object sender, RoutedEventArgs e)
        {
            if(int.TryParse(AmountTextBox.Text, out delta))
            {
                if (delta > 0)
                {
                    int newAmount = originalAmount + delta;
                    sqlQuery = "UPDATE TABLE T_Videos SET amountTotal = " + newAmount +
                        "WHERE p_vid_id = " + id;
                    SqlCommand sqlcmd = new SqlCommand(sqlQuery, con);
                    con.Open();
                    sqlcmd.ExecuteNonQuery();
                    con.Close();
                    Close();
                }
            }         
        }

        private void DecreaseButton_Click(object sender, RoutedEventArgs e)
        {
            if (int.TryParse(AmountTextBox.Text, out delta))
            {
                if (delta > 0 && originalAmount >= delta)
                {
                    int newAmount = originalAmount - delta;
                    sqlQuery = "UPDATE TABLE T_Videos SET amountTotal = " + newAmount +
                        "WHERE p_vid_id = " + id;
                    SqlCommand sqlcmd = new SqlCommand(sqlQuery, con);
                    con.Open();
                    sqlcmd.ExecuteNonQuery();
                    con.Close();
                    Close();
                }
            }
                
        }

        
    }
}
