﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace VideoManager_4
{
    /// <summary>
    /// Interaction logic for CustomerInfoWindow.xaml
    /// </summary>
    public partial class CustomerInfoWindow : Window
    {
        string sqlQuery;
        DataSet ds;
        public int id { get; set; }
        public CustomerInfoWindow(int id)
        {
            InitializeComponent();
            this.id = id;
        }

        private void Save_Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult res = MessageBox.Show("Do you want to save the changes?", 
                "Question", MessageBoxButton.YesNo, 
                MessageBoxImage.Question, MessageBoxResult.No);
            switch(res)
            {
                case MessageBoxResult.Yes:
                    SaveChanges();
                    break;
                case MessageBoxResult.No:
                    break;
            }
        }

        private void SaveChanges()
        {
            string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            SqlConnection con = new SqlConnection(cs);

        }

        private void Cancel_Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ID_TextBlock.Text = id.ToString();
            string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            SqlConnection con = new SqlConnection(cs);
            sqlQuery = "SELECT * FROM T_Customers WHERE p_customer_nr = " + ID_TextBlock.Text;
            SqlDataAdapter da = new SqlDataAdapter(sqlQuery, con);
            ds = new DataSet();
            da.Fill(ds, "Customers");

            
            DataRow dr = ds.Tables["Customers"].Rows[0];
                
            FirstName_TextBox.Text = dr["firstName"].ToString();
            LastName_TextBox.Text = dr["lastName"].ToString();
            Street_TextBox.Text = dr["street"].ToString();
            PostalCode_TextBox.Text = dr["postalCode"].ToString();
            City_TextBox.Text = dr["city"].ToString();
            DateTime bd = (DateTime)dr["birthdate"];
            BirthDate_TextBox.Text = bd.ToShortDateString();
            if (dr["title"].ToString() == "Male")
                Title_ComboBox.SelectedValue = "Male";
            else
                Title_ComboBox.SelectedValue = "Female";

        }
    }
}
