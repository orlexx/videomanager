﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using Microsoft.Win32;

namespace VideoManager_4
{
    /// <summary>
    /// Interaction logic for AddNewVideo_Window1.xaml
    /// </summary>
    public partial class AddNewVideo_Window1 : Window
    {
        SqlDataAdapter adapter;
        DataTable customersTable;
        static string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        SqlConnection connection = new SqlConnection(cs);
        SqlCommand sqlcmd = new SqlCommand();
        string picPath;
        string sqlQuery;
        DataSet ds;
        public AddNewVideo_Window1()
        {
            InitializeComponent();
        }

        private void Save_Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult res = MessageBox.Show("Do you want to save the changes?",
                "Question", MessageBoxButton.YesNo,
                MessageBoxImage.Question, MessageBoxResult.No);
            switch (res)
            {
                case MessageBoxResult.Yes:
                    SaveChanges();
                    break;
                case MessageBoxResult.No:
                    break;
            }
        }
        private void SaveChanges()
        {
            string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            SqlConnection con = new SqlConnection(cs);

            string title = Title_TextBox.Text;
            string storage = Storage_ComboBox.SelectedValue.ToString();
            string category = Category_ComboBox.SelectedValue.ToString();
            string ageRestriction = AgeRestriction_TextBox.Text;
            string releaseYear = ReleaseYear_TextBox.Text;
            string videoDescription = Description_TextBox.Text;
            string amountTotal = AmountTotal_TextBox.Text;

            MemoryStream ms = new MemoryStream();
            byte[] img = null;
            if (PosterImage.Source.ToString() != "Assets\\camera.png")
            {
                FileStream fs = new FileStream(picPath, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                img = br.ReadBytes((int)fs.Length);
            }
                

            string sqlEx = "sp_InsertVideo";
            string id = ID_TextBlock.Text;

            con.Open();
            SqlCommand command = new SqlCommand(sqlEx, con);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter titleParam = new SqlParameter
            {
                ParameterName = "@title",
                Value = title
            };
            command.Parameters.Add(titleParam);

            SqlParameter idParam = new SqlParameter
            {
                ParameterName = "@p_vid_nr",
                SqlDbType = SqlDbType.Int,
                Direction = ParameterDirection.Output
            };
            command.Parameters.Add(idParam);

            SqlParameter storageParam = new SqlParameter
            {
                ParameterName = "@storage",
                Value = storage
            };
            command.Parameters.Add(storageParam);

            SqlParameter categoryParam = new SqlParameter
            {
                ParameterName = "@category",
                Value = category
            };
            command.Parameters.Add(categoryParam);

            SqlParameter ageRestrictionParam = new SqlParameter
            {
                ParameterName = "@ageRestriction",
                Value = ageRestriction
            };
            command.Parameters.Add(ageRestrictionParam);

            SqlParameter releaseYearParam = new SqlParameter
            {
                ParameterName = "@releaseYear",
                Value = releaseYear
            };
            command.Parameters.Add(releaseYearParam);

            SqlParameter descriptParam = new SqlParameter
            {
                ParameterName = "@videoDescription",
                Value = videoDescription
            };
            command.Parameters.Add(descriptParam);

            SqlParameter amountParam = new SqlParameter
            {
                ParameterName = "@amountTotal",
                Value = amountTotal
            };
            command.Parameters.Add(amountParam);

            SqlParameter posterParam = new SqlParameter();
            posterParam.ParameterName = "@poster";
            //{
            //    ParameterName = ,
            //    Value = img                
            //};
            if (PosterImage.Source.ToString() == "Assets\\camera.png")
                posterParam.Value = null;
            else
                posterParam.Value = img;
            command.Parameters.Add(posterParam);

            var result = command.ExecuteNonQuery();
            MessageBox.Show("A new video (" + title + ", " + releaseYear +
                ") has been added");
            ID_TextBlock.Text = idParam.Value.ToString();
        }

        private void Cancel_Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ChgImg_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog ofdImage = new OpenFileDialog();
                ofdImage.Filter = "JPEG Files|*.jpg|Bitmap Files|*.bmp|Gif Files|*.gif";
                ofdImage.DefaultExt = "jpg";
                ofdImage.Title = "Select poster";
                ofdImage.FilterIndex = 1;
                if (ofdImage.ShowDialog() == true)
                {
                    picPath = ofdImage.FileName.ToString();

                    //Stream s = ofdImage.OpenFile();
                    PosterImage.Source = new BitmapImage(new Uri(ofdImage.FileName));
                    //picPath = ofdImage.SafeFileName.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Category_ComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            List<string> data = new List<string>();
            data.Add("Cartoon");
            data.Add("Drama");
            data.Add("Action");
            data.Add("Adventure");
            data.Add("Detective fiction");
            data.Add("Documentary");
            data.Add("Fantasy");
            data.Add("Science-fiction");
            data.Add("Horror");
            data.Add("Thriller");
            data.Add("Comedy");
            data.Add("Educational");
            var comboBox = sender as ComboBox;
            comboBox.ItemsSource = data;
        }

        private void Storage_ComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            List<string> data = new List<string>();
            data.Add("VHS");
            data.Add("DVD");
            data.Add("bluray");
            var comboBox = sender as ComboBox;
            comboBox.ItemsSource = data;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
