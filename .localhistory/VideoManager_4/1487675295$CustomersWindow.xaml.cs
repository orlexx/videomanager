﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using Microsoft.Win32;

namespace VideoManager_4
{
    /// <summary>
    /// Interaction logic for CustomersWindow.xaml
    /// </summary>
    public partial class CustomersWindow : Window
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        SqlDataAdapter adapter;
        SqlConnection connection = new SqlConnection(connectionString);
        DataTable customersTable;
        public int videoId { get; set; }
        public int customerId { get; set; }

        public CustomersWindow()
        {
            InitializeComponent();
        }

        public CustomersWindow(int videoId)
        {
            InitializeComponent();
            this.videoId = videoId;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string sqlEx = "SELECT * FROM T_Customers";
            customersTable = new DataTable();
            
            SqlCommand command = new SqlCommand(sqlEx, connection);
            adapter = new SqlDataAdapter(command);
            connection.Open();
            adapter.Fill(customersTable);
            customersGrid.ItemsSource = customersTable.DefaultView;
                 
            if (connection != null)
                connection.Close();

        }

        private void findButton_Click(object sender, RoutedEventArgs e)
        {
            string sqlEx = "SELECT * FROM T_Customers WHERE firstName LIKE '%' + @name + '%' OR lastName LIKE '%' + @name + '%'";
            string name = findTextBox.Text;
            customersTable = new DataTable();
            
            SqlCommand command = new SqlCommand(sqlEx, connection);
            SqlParameter nameParam = new SqlParameter
            {
                ParameterName = "@name",
                Value = name
            };
            command.Parameters.Add(nameParam);
            adapter = new SqlDataAdapter(command);
            connection.Open();
            adapter.Fill(customersTable);
            customersGrid.ItemsSource = customersTable.DefaultView;

            if (connection != null)
                connection.Close();
        }

        private void infoButton_Click(object sender, RoutedEventArgs e)
        {            
            string idString = ((DataRowView)(customersGrid.SelectedItem)).Row.ItemArray[0].ToString();
            int id = Convert.ToInt32(idString);
            CustomerInfoWindow ciw = new CustomerInfoWindow(id);
            ciw.Show();
        }

        private void customersGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            infoButton.IsEnabled = true;
            if (customersGrid.SelectedItem != null)
            {
                string idString = ((DataRowView)(customersGrid.SelectedItem)).Row.ItemArray[0].ToString();
                customerId = Convert.ToInt32(idString);
            }
        }

        private void AddNew_Button_Click(object sender, RoutedEventArgs e)
        {
            AddnewUserWindow anrWindor = new AddnewUserWindow();
            anrWindor.Show();
        }

        private void borrow_Button_Click(object sender, RoutedEventArgs e)
        {

            int borrowedVideos;
            string sqlQuery2 = "SELECT COUNT(fp_customer_nr) FROM T_Customers_Videos WHERE fp_customer_nr = " + customerId;

            connection.Open();
            //if (videoGrid.SelectedItem != null)                                             //Sieht sinnlos aus, aber anders geht nicht.
            //{
            //    SqlCommand sqlcmd2 = new SqlCommand(sqlQuery2, connection);
            //    Int32.TryParse(sqlcmd2.ExecuteScalar().ToString(), out borrowedVideos);
            //}
            //else
            //{
                SqlCommand sqlcmd2 = new SqlCommand(sqlQuery2, connection);
                Int32.TryParse(sqlcmd2.ExecuteScalar().ToString(), out borrowedVideos);
            //}
            connection.Close();


            if (borrowedVideos < 5)
            {
                int availableAmount;
                int vidBorrowedByCustomer;
                DateTime birthdate;
                int ageRestriction;

                string sqlQuery1 = "SELECT amountAvailable FROM T_Videos WHERE " +
                    "p_vid_nr = " + videoId.ToString();
                string sqlQuery5 = "SELECT COUNT (*) FROM T_Customers_Videos WHERE fp_customer_nr = "
                    + customerId + " AND fp_vid_nr = " + videoId;
                string sqlQuery3 = "SELECT  birthdate from T_Customers WHERE p_customer_nr = " + customerId;
                string sqlQuery4 = "SELECT ageRestriction from T_Videos WHERE p_vid_nr = " + videoId;

                connection = new SqlConnection(connectionString);
                SqlCommand sqlcmd1 = new SqlCommand(sqlQuery1, connection);
                SqlCommand sqlcmd5 = new SqlCommand(sqlQuery5, connection);
                SqlCommand sqlcmd3 = new SqlCommand(sqlQuery3, connection);
                SqlCommand sqlcmd4 = new SqlCommand(sqlQuery4, connection);
                connection.Open();
                Int32.TryParse(sqlcmd1.ExecuteScalar().ToString(), out availableAmount);
                Int32.TryParse(sqlcmd5.ExecuteScalar().ToString(), out vidBorrowedByCustomer);
                DateTime.TryParse(sqlcmd3.ExecuteScalar().ToString(), out birthdate);
                Int32.TryParse(sqlcmd4.ExecuteScalar().ToString(), out ageRestriction);

                int years = DateTime.Now.Year - birthdate.Year;
                birthdate = birthdate.AddYears(years);
                if (DateTime.Now.CompareTo(birthdate) < 0) years--;
                if (years >= ageRestriction)
                {
                    if (vidBorrowedByCustomer == 0)
                    {
                        if (availableAmount > 0)
                        {
                            string sqlEx = "sp_InsertCustomer_Video";

                            SqlCommand sqlcmd = new SqlCommand(sqlEx, connection);
                            sqlcmd.CommandType = CommandType.StoredProcedure;

                            string borrowDate = DateTime.Now.ToShortDateString();

                            SqlParameter customerIdParam = new SqlParameter
                            {
                                ParameterName = "@fp_customer_nr",
                                Value = customerId
                            };
                            sqlcmd.Parameters.Add(customerIdParam);

                            SqlParameter videoIdParam = new SqlParameter
                            {
                                ParameterName = "@fp_vid_nr",
                                Value = videoId
                            };
                            sqlcmd.Parameters.Add(videoIdParam);

                            SqlParameter borrowDateParam = new SqlParameter
                            {
                                ParameterName = "@borrowDate",
                                Value = DateTime.Now
                            };
                            sqlcmd.Parameters.Add(borrowDateParam);

                            SqlParameter returnDateParam = new SqlParameter
                            {
                                ParameterName = "@returnDate",
                                Value = DateTime.Now.AddDays(5)
                            };
                            sqlcmd.Parameters.Add(returnDateParam);


                            using (connection)
                            {
                                sqlcmd.ExecuteNonQuery();
                                //availableAmount--;
                                //sqlQuery1 = "UPDATE T_Videos SET amountAvailable = " + availableAmount +
                                //        "WHERE p_vid_nr = " + videoId;
                                //sqlcmd1 = new SqlCommand(sqlQuery1, connection);
                                //sqlcmd1.ExecuteNonQuery();
                            }
                            MessageBox.Show("Video borrowed successfully. Has to be returned on " + DateTime.Now.AddDays(5).ToShortDateString());

                            Close();
                        }
                        else MessageBox.Show("Sorry, this video is not available.");
                    }
                    else MessageBox.Show("Sorry, you cannot borrow the same video twice at the same time, no matter how great this movie is.");
                }
                else MessageBox.Show("Sorry, this video is inappropriate for your age.");
                UpdateAvailableAmount();
                connection.Close();
            }
            else MessageBox.Show("Sorry, you cannot borrow more than five videos at the same time.");
            
        }

        public void UpdateAvailableAmount()
        {
            int totalAmount;
            int availableAmount;
            int borrowedAmount;
            string sqlQuery = "SELECT amountTotal FROM T_Videos WHERE p_vid_nr = " + videoId;
            string sqlQuery1 = "SELECT COUNT (*) FROM T_Customers_Videos WHERE fp_vid_nr = " + videoId;
            SqlCommand sqlcmd = new SqlCommand(sqlQuery, connection);
            SqlCommand sqlcmd1 = new SqlCommand(sqlQuery1, connection);
            connection.ConnectionString = connectionString;
            using (connection)
            {
                connection.Open();
                Int32.TryParse(sqlcmd.ExecuteScalar().ToString(), out totalAmount);
                Int32.TryParse(sqlcmd1.ExecuteScalar().ToString(), out borrowedAmount);
            }
                
            availableAmount = totalAmount - borrowedAmount;
            string sqlQuery2 = "UPDATE T_Videos SET amountAvailable = " + availableAmount +
                " WHERE p_vid_nr = " + videoId; ;
            SqlCommand sqlcmd2 = new SqlCommand(sqlQuery2, connection);
            connection.Open();
            sqlcmd2.ExecuteNonQuery();
            connection.Close();
        }
    }
}

