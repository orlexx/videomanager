﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using Microsoft.Win32;

namespace VideoManager_4
{
    /// <summary>
    /// Interaction logic for VideoInfo_Window.xaml
    /// </summary>
    public partial class VideoInfo_Window : Window
    {
        SqlDataAdapter adapter;
        DataTable customersTable;
        static string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        SqlConnection connection = new SqlConnection(cs);
        SqlCommand sqlcmd = new SqlCommand();
        string picPath;
        string sqlQuery;
        DataSet ds;
        public int customerId { get; set; }
        public int id { get; set; }
        public VideoInfo_Window(int id)
        {
            InitializeComponent();
            this.id = id;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {            
            ID_TextBlock.Text = id.ToString();
            
            sqlQuery = "SELECT * FROM T_Videos WHERE p_vid_nr = " + id;
            SqlDataAdapter da = new SqlDataAdapter(sqlQuery, connection);
            ds = new DataSet();
            da.Fill(ds, "Videos");
            DataRow dr = ds.Tables["Videos"].Rows[0];
            Title_TextBox.Text = dr["title"].ToString();
            Storage_ComboBox.SelectedValue = dr["storage"].ToString();
            Category_ComboBox.SelectedValue = dr["category"].ToString();
            AgeRestriction_TextBox.Text = dr["ageRestriction"].ToString();
            ReleaseYear_TextBox.Text = dr["releaseYear"].ToString();
            Description_TextBox.Text = dr["videoDescription"].ToString();
            AmountTotal_TextBlock.Text = dr["amountTotal"].ToString();
            //AmountAvailable_TextBlock.Text = dr["amountAvailable"].ToString();
            int availableAmount;
            string sqlQuery1 = "SELECT amountAvailable FROM T_Videos WHERE " +
                "p_vid_nr = " + id.ToString();
            SqlCommand sqlcmd1 = new SqlCommand(sqlQuery1, connection);
            connection.Open();
            Int32.TryParse(sqlcmd1.ExecuteScalar().ToString(), out availableAmount);
            Borrow_button.IsEnabled = false;
            AmountAvailable_TextBlock.Text = availableAmount.ToString();

            
            MemoryStream stream = new MemoryStream();
            sqlcmd = new SqlCommand("SELECT poster FROM T_Videos WHERE p_vid_nr = " + id, connection);
            byte[] image = null;
            if (sqlcmd.ExecuteScalar() != DBNull.Value)
            {
                image = (byte[])sqlcmd.ExecuteScalar();
            }
            
            if(image != null)
            {
                stream.Write(image, 0, image.Length);
                connection.Close();
                MemoryStream ms = new MemoryStream(image);
                BitmapImage bmi = new BitmapImage();
                bmi.BeginInit();
                bmi.StreamSource = ms;
                bmi.EndInit();
                PosterImage.Source = bmi;
                ChgImg_Button.Content = "Change poster";
            }
            else
                ChgImg_Button.Content = "Add poster";
            LoadCustomers();
            Info_TextBlock.Text = Title_TextBox.Text + " (" + ReleaseYear_TextBox.Text +
                ") has been borrowed by following customers:";
        }
        public VideoInfo_Window()
        {
            InitializeComponent();
        }
        
        private void Category_ComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            List<string> data = new List<string>();
            data.Add("Cartoon");
            data.Add("Drama");
            data.Add("Action");
            data.Add("Adventure");
            data.Add("Detective fiction");
            data.Add("Documentary");
            data.Add("Fantasy");
            data.Add("Science-fiction");
            data.Add("Horror");
            data.Add("Thriller");
            data.Add("Comedy");
            data.Add("Educational");

            // ... Get the ComboBox reference.
            var comboBox = sender as ComboBox;

            //... Assign the ItemsSource to the List.
            comboBox.ItemsSource = data;
        }

        private void Storage_ComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            List<string> data = new List<string>();
            data.Add("VHS");
            data.Add("DVD");
            data.Add("bluray");

            // ... Get the ComboBox reference.
            var comboBox = sender as ComboBox;

            //... Assign the ItemsSource to the List.
            comboBox.ItemsSource = data;
        }

        private void Save_Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult res = MessageBox.Show("Do you want to save the changes?",
                "Question", MessageBoxButton.YesNo,
                MessageBoxImage.Question, MessageBoxResult.No);
            switch (res)
            {
                case MessageBoxResult.Yes:
                    SaveChanges();
                    break;
                case MessageBoxResult.No:
                    break;
            }
        }

        private void Cancel_Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Delete_Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult res = MessageBox.Show("Do you want to delete this video?",
               "Question", MessageBoxButton.YesNo,
               MessageBoxImage.Question, MessageBoxResult.No);
            switch (res)
            {
                case MessageBoxResult.Yes:
                    DeleteVideo();
                    break;
                case MessageBoxResult.No:
                    break;
            }
        }
        private void DeleteVideo()
        {
            using (connection)
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.CommandText = "DELETE FROM T_Videos WHERE p_vid_nr = " + id;
                command.Connection = connection;
                command.ExecuteNonQuery();
            }
            ID_TextBlock.Foreground = Brushes.Gray;
            Title_TextBox.IsEnabled = false;
            Storage_ComboBox.IsEnabled = false;
            Category_ComboBox.IsEnabled = false;
            AgeRestriction_TextBox.IsEnabled = false;
            ReleaseYear_TextBox.IsEnabled = false;
            Description_TextBox.IsEnabled = false;
            MessageBox.Show("This video has been removed.");
            
        }
        private void SaveChanges()
        {
            string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            SqlConnection con = new SqlConnection(cs);

            string title = Title_TextBox.Text;
            string storage = Storage_ComboBox.SelectedValue.ToString();
            string category = Category_ComboBox.SelectedValue.ToString();
            string ageRestriction = AgeRestriction_TextBox.Text;
            string releaseYear = ReleaseYear_TextBox.Text;
            string videoDescription = Description_TextBox.Text;

            MemoryStream ms = new MemoryStream();
            
            byte[] img;
            FileStream fs = new FileStream(picPath, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            img = br.ReadBytes((int)fs.Length);

            string sqlEx = "sp_UpdateVideo";
            string id = ID_TextBlock.Text;

            con.Open();
            SqlCommand command = new SqlCommand(sqlEx, con);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter titleParam = new SqlParameter
            {
                ParameterName = "@title",
                Value = title
            };
            command.Parameters.Add(titleParam);

            SqlParameter idParam = new SqlParameter
            {
                ParameterName = "@p_vid_nr",
                Value = id
            };
            command.Parameters.Add(idParam);

            SqlParameter storageParam = new SqlParameter
            {
                ParameterName = "@storage",
                Value = storage
            };
            command.Parameters.Add(storageParam);

            SqlParameter categoryParam = new SqlParameter
            {
                ParameterName = "@category",
                Value = category
            };
            command.Parameters.Add(categoryParam);

            SqlParameter ageRestrictionParam = new SqlParameter
            {
                ParameterName = "@ageRestriction",
                Value = ageRestriction
            };
            command.Parameters.Add(ageRestrictionParam);

            SqlParameter releaseYearParam = new SqlParameter
            {
                ParameterName = "@releaseYear",
                Value = releaseYear
            };
            command.Parameters.Add(releaseYearParam);

            SqlParameter descriptParam = new SqlParameter
            {
                ParameterName = "@videoDescription",
                Value = videoDescription
            };
            command.Parameters.Add(descriptParam);

            SqlParameter posterParam = new SqlParameter
            {
                ParameterName = "@poster",
                Value = img
            };
            command.Parameters.Add(posterParam);

            var result = command.ExecuteNonQuery();
            MessageBox.Show("Updated.");
            
        }

        private void ChgAmount_Button_Click(object sender, RoutedEventArgs e)
        {
            ChgAmountOfVideosWindow caw = new ChgAmountOfVideosWindow(Convert.ToInt32(ID_TextBlock.Text));
            caw.Show();

        }
        private void LoadCustomers()
        {
            string sqlEx = "SELECT T_Customers.p_customer_nr, T_Customers.firstName, T_Customers.lastName, T_Customers_Videos.borrowDate, T_Customers_Videos.returnDate " +
                            "FROM T_Customers, T_Customers_Videos " +
                            "WHERE T_Customers_Videos.fp_customer_nr = T_Customers.p_customer_nr " +
                            "AND T_Customers_Videos.fp_vid_nr = " + id.ToString();
            customersTable = new DataTable();
            sqlcmd = new SqlCommand(sqlEx, connection);
            adapter = new SqlDataAdapter(sqlcmd);
            //connection.ConnectionString = cs;
            using (connection)
            {
                //connection.Open();
                adapter.Fill(customersTable);
                //connection.Close();
            }
            customersGrid.ItemsSource = customersTable.DefaultView;
        }

        private void customersGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Borrow_button.IsEnabled = true;
            if (customersGrid.SelectedItem != null)
            {
                if (customersGrid.Items.Count != 0)
                {
                    string idString = ((DataRowView)(customersGrid.SelectedItem)).Row.ItemArray[0].ToString();
                    customerId = Convert.ToInt32(idString);
                }
            }
        }

        private void Borrow_button_Click(object sender, RoutedEventArgs e)
        {
            CustomersWindow cw = new CustomersWindow(id);
            cw.Show();
        }

        //private void Return_button_Click(object sender, RoutedEventArgs e)
        //{
        //    if (customersGrid.SelectedItem == null)
        //        Borrow_button.IsEnabled = false;
        //    else
        //    {
        //        string sqlEx = "DELETE FROM T_Customers_Videos WHERE fp_customer_nr = " 
        //            + customerId + " AND fp_vid_nr = " + id;
        //        sqlcmd = new SqlCommand(sqlEx, connection);
        //        connection.Open();
        //        sqlcmd.ExecuteNonQuery();
        //        MessageBox.Show("Video returned.");
        //        connection.Close();
        //        //Return_button.IsEnabled = false;
        //        UpdateAvailableAmount();
        //    }
        //}

        private void Refresh_button_Click(object sender, RoutedEventArgs e)
        {
            LoadCustomers();
        }
        public void UpdateAvailableAmount()
        {
            int totalAmount;
            int availableAmount;
            int borrowedAmount;
            string sqlQuery = "SELECT amountTotal FROM T_Videos WHERE p_vid_nr = " + id;
            string sqlQuery1 = "SELECT COUNT (*) FROM T_Customers_Videos WHERE fp_vid_nr = " + id;
            SqlCommand sqlcmd = new SqlCommand(sqlQuery, connection);
            SqlCommand sqlcmd1 = new SqlCommand(sqlQuery1, connection);
            connection.Open();
            Int32.TryParse(sqlcmd.ExecuteScalar().ToString(), out totalAmount);
            Int32.TryParse(sqlcmd1.ExecuteScalar().ToString(), out borrowedAmount);
            connection.Close();
            availableAmount = totalAmount - borrowedAmount;
            string sqlQuery2 = "UPDATE T_Videos SET amountAvailable = " + availableAmount +
                " WHERE p_vid_nr = " + id; ;
            SqlCommand sqlcmd2 = new SqlCommand(sqlQuery2, connection);
            connection.Open();
            sqlcmd2.ExecuteNonQuery();
            connection.Close();
        }

        private void ChgImg_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog ofdImage = new OpenFileDialog();
                ofdImage.Filter = "JPEG Files|*.jpg|Bitmap Files|*.bmp|Gif Files|*.gif";
                ofdImage.DefaultExt = "jpg";
                ofdImage.Title = "Select poster";
                ofdImage.FilterIndex = 1;
                if (ofdImage.ShowDialog() == true)
                {
                    picPath = ofdImage.FileName.ToString();
                    
                    //Stream s = ofdImage.OpenFile();
                    PosterImage.Source = new BitmapImage(new Uri(ofdImage.FileName));
                    //picPath = ofdImage.SafeFileName.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
