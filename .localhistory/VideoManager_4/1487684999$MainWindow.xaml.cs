﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace VideoManager_4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        SqlDataAdapter adapter;
        DataTable videoTable;
        DataTable customersTable;
        SqlConnection connection;
        SqlCommand command;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadVideos();
            LoadCustomers();
        }
            
        private void videoGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            VideoInfo_Button.IsEnabled = true;
        }
        
        private void AddNew_Button_Click(object sender, RoutedEventArgs e)
        {
            AddnewUserWindow anrWindor = new AddnewUserWindow();
            anrWindor.Show();
        }

        private void customersGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CustomerInfo_Button.IsEnabled = true;
        }

        private void CustomerInfoButton_Click(object sender, RoutedEventArgs e)
        {
            string idString = ((DataRowView)(customersGrid.SelectedItem)).Row.ItemArray[0].ToString();
            int id = Convert.ToInt32(idString);
            CustomerInfoWindow ciw = new CustomerInfoWindow(id);
            ciw.Show();
        }

        private void VideoInfo_Button_Click(object sender, RoutedEventArgs e)
        {
            if (videoGrid.SelectedItem != null)
            {
                string idString = ((DataRowView)(videoGrid.SelectedItem)).Row.ItemArray[0].ToString();
                int id = Convert.ToInt32(idString);
                VideoInfo_Window viw = new VideoInfo_Window(id);
                viw.Show();
            }
            
        }

        private void findVideoButton_Click(object sender, RoutedEventArgs e)
        {
            string sqlEx = "SELECT * FROM T_Videos WHERE title LIKE '%' + @title + '%'";
            string title = findTextBox.Text;
            videoTable = new DataTable();
            SqlConnection connection = null;

            connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sqlEx, connection);
            SqlParameter nameParam = new SqlParameter
            {
                ParameterName = "@title",
                Value = title
            };
            command.Parameters.Add(nameParam);
            adapter = new SqlDataAdapter(command);
            connection.Open();
            adapter.Fill(videoTable);
            videoGrid.ItemsSource = videoTable.DefaultView;

            if (connection != null)
                connection.Close();
        }

        private void findCustomerButton_Click(object sender, RoutedEventArgs e)
        {
            string sqlEx = "SELECT * FROM T_Customers WHERE firstName LIKE '%' + @name + '%' OR lastName LIKE '%' + @name + '%'";
            string name = findTextBox.Text;
            customersTable = new DataTable();
            SqlConnection connection = null;
                connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sqlEx, connection);
            SqlParameter nameParam = new SqlParameter
            {
                ParameterName = "@name",
                Value = name
            };
            command.Parameters.Add(nameParam);
            adapter = new SqlDataAdapter(command);
            connection.Open();
            adapter.Fill(customersTable);
            customersGrid.ItemsSource = customersTable.DefaultView;

            if (connection != null)
                connection.Close();
        }
        private void LoadCustomers()
        {
            string selCustomers = "SELECT * FROM T_Customers";
            customersTable = new DataTable();
            connection = null;

            connection = new SqlConnection(connectionString);
            command = new SqlCommand(selCustomers, connection);
            adapter = new SqlDataAdapter(command);
            connection.Open();
            adapter.Fill(customersTable);
            customersGrid.ItemsSource = customersTable.DefaultView;

            if (connection != null)
                connection.Close();
        }
        private void LoadVideos()
        {
            CustomerInfo_Button.IsEnabled = false;
            VideoInfo_Button.IsEnabled = false;
            string sqlEx = "SELECT * FROM T_Videos";
            videoTable = new DataTable();
            connection = new SqlConnection(connectionString);
            command = new SqlCommand(sqlEx, connection);
            adapter = new SqlDataAdapter(command);
            connection.Open();
            adapter.Fill(videoTable);
            videoGrid.ItemsSource = videoTable.DefaultView;

            if (connection != null)
                connection.Close();
        }

        private void RefreshCustomers_Button_Click(object sender, RoutedEventArgs e)
        {
            LoadCustomers();
        }

        private void AddNewVideo_Button_Click(object sender, RoutedEventArgs e)
        {
            AddNewVideo_Window1 anvw = new AddNewVideo_Window1();
            anvw.Show();
        }

        private void RefreshVideos_Button_Click(object sender, RoutedEventArgs e)
        {
            LoadVideos();
        }
    }
}
