﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace VideoManager_4
{
    /// <summary>
    /// Interaction logic for VideoInfo_Window.xaml
    /// </summary>
    public partial class VideoInfo_Window : Window
    {
        string sqlQuery;
        DataSet ds;
        public int id { get; set; }
        public VideoInfo_Window(int id)
        {
            InitializeComponent();
            this.id = id;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ID_TextBlock.Text = id.ToString();
            string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            SqlConnection con = new SqlConnection(cs);
            sqlQuery = "SELECT * FROM T_Videos WHERE p_vid_nr = " + id;
            SqlDataAdapter da = new SqlDataAdapter(sqlQuery, con);
            ds = new DataSet();
            da.Fill(ds, "Videos");

            DataRow dr = ds.Tables["Videos"].Rows[0];
            Title_TextBox.Text = dr["title"].ToString();
            Storage_ComboBox.SelectedValue = dr["storage"].ToString();
            Category_ComboBox.SelectedValue = dr["category"].ToString();
            AgeRestriction_TextBox.Text = dr["ageRestriction"].ToString();
            ReleaseYear_TextBox.Text = dr["releaseYear"].ToString();
            Description_TextBox.Text = dr["videoDescription"].ToString();
        }
        public VideoInfo_Window()
        {
            InitializeComponent();
        }
        
        private void Category_ComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            List<string> data = new List<string>();
            data.Add("Cartoon");
            data.Add("Drama");
            data.Add("Action");
            data.Add("Adventure");
            data.Add("Detective fiction");
            data.Add("Documentary");
            data.Add("Fantasy");
            data.Add("Science-fiction");
            data.Add("Horror");
            data.Add("Thriller");
            data.Add("Comedy");
            data.Add("Educational");

            // ... Get the ComboBox reference.
            var comboBox = sender as ComboBox;

            //... Assign the ItemsSource to the List.
            comboBox.ItemsSource = data;
        }

        private void Storage_ComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            List<string> data = new List<string>();
            data.Add("VHS");
            data.Add("DVD");
            data.Add("bluray");

            // ... Get the ComboBox reference.
            var comboBox = sender as ComboBox;

            //... Assign the ItemsSource to the List.
            comboBox.ItemsSource = data;
        }

        private void Save_Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Cancel_Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Delete_Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
