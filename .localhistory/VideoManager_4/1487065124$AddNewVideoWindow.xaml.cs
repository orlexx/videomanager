﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace VideoManager_4
{
    /// <summary>
    /// Interaction logic for AddNewVideoWindow.xaml
    /// </summary>
    public partial class AddNewVideoWindow : Window
    {
        public AddNewVideoWindow()
        {
            InitializeComponent();
        }

        private void Cancel_Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Save_Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult res = MessageBox.Show("Do you want to save the changes?",
               "Question", MessageBoxButton.YesNo,
               MessageBoxImage.Question, MessageBoxResult.No);
            switch (res)
            {
                case MessageBoxResult.Yes:
                    if (Title_TextBox.Text != null &&
                        AgeRestriction_TextBox.Text != null &&
                        ReleaseYear_TextBox.Text != null)
                        SaveChanges();
                    else
                        MessageBox.Show("Please type in the video data completely.");
                    break;
                case MessageBoxResult.No:
                    break;
            }
        }

        private void SaveChanges()
        {
            string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            SqlConnection con = new SqlConnection(cs);

            int ageRestInt;
            int relYearInt;

            string title = Title_TextBox.Text;
            string storage = Storage_ComboBox.SelectedValue.ToString();
            string category = Category_ComboBox.SelectedValue.ToString();
            string ageRestriction = AgeRestriction_TextBox.Text;
            string releaseYear = ReleaseYear_TextBox.Text;
            string videoDescription = Description_TextBox.Text;

            string sqlEx = "sp_InsertVideo";
            

            if (!(int.TryParse(ageRestriction, out ageRestInt) &&
                int.TryParse(releaseYear, out relYearInt)))
            {

                con.Open();
                SqlCommand command = new SqlCommand(sqlEx, con);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter titleParam = new SqlParameter
                {
                    ParameterName = "@title",
                    Value = title
                };
                command.Parameters.Add(titleParam);

                SqlParameter storageParam = new SqlParameter
                {
                    ParameterName = "@storage",
                    Value = storage
                };
                command.Parameters.Add(storageParam);

                SqlParameter categoryParam = new SqlParameter
                {
                    ParameterName = "@category",
                    Value = category
                };
                command.Parameters.Add(categoryParam);

                SqlParameter ageRestrictionParam = new SqlParameter
                {
                    ParameterName = "@ageRestriction",
                    Value = ageRestriction
                };
                command.Parameters.Add(ageRestrictionParam);

                SqlParameter releaseYearParam = new SqlParameter
                {
                    ParameterName = "@releaseYear",
                    Value = releaseYear
                };
                command.Parameters.Add(releaseYearParam);

                SqlParameter descriptParam = new SqlParameter
                {
                    ParameterName = "@videoDescription",
                    Value = videoDescription
                };
                command.Parameters.Add(descriptParam);

                SqlParameter idParam = new SqlParameter
                {
                    ParameterName = "@p_vid_nr",
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Output
                };
                command.Parameters.Add(idParam);

                command.ExecuteNonQuery();
                string videoId = idParam.Value.ToString();
                ID_TextBlock.Text = videoId;
                Message_TextBlock.Text = "New video has been added.";
            }
            else MessageBox.Show("Please check the input format.");
        }

        private void Storage_ComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            List<string> data = new List<string>();
            data.Add("DVD");
            data.Add("VHS");
            data.Add("bluray");

            // ... Get the ComboBox reference.
            var comboBox = sender as ComboBox;

            //... Assign the ItemsSource to the List.
            comboBox.ItemsSource = data;

        }

        private void Category_ComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            List<string> data = new List<string>();
            data.Add("Cartoon");
            data.Add("Drama");
            data.Add("Action");
            data.Add("Adventure");
            data.Add("Detective fiction");
            data.Add("Documentary");
            data.Add("Fantasy");
            data.Add("Science-fiction");
            data.Add("Horror");
            data.Add("Thriller");
            data.Add("Comedy");
            data.Add("Educational");

            // ... Get the ComboBox reference.
            var comboBox = sender as ComboBox;

            //... Assign the ItemsSource to the List.
            comboBox.ItemsSource = data;
        }
    }
}
