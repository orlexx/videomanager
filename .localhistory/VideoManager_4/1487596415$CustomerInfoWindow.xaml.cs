﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using Microsoft.Win32;

namespace VideoManager_4
{
    /// <summary>
    /// Interaction logic for CustomerInfoWindow.xaml
    /// </summary>
    public partial class CustomerInfoWindow : Window
    {        
        SqlDataAdapter adapter;
        DataTable videoTable;
        static string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        SqlConnection connection = new SqlConnection(cs);
        SqlCommand sqlcmd = new SqlCommand();
        string picPath;
        string sqlQuery;
        DataSet ds;
        public int customerId { get; set; }
        public int videoId { get; set; }
        public CustomerInfoWindow(int id)
        {
            InitializeComponent();
            this.customerId = id;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Return_button.IsEnabled = false;
            Info_TextBlock.Text = "";
            ID_TextBlock.Text = customerId.ToString();
            sqlQuery = "SELECT * FROM T_Customers WHERE p_customer_nr = " + ID_TextBlock.Text;
            SqlDataAdapter da = new SqlDataAdapter(sqlQuery, connection);
            ds = new DataSet();
            da.Fill(ds, "Customers");
            
            DataRow dr = ds.Tables["Customers"].Rows[0];
            Title_ComboBox.SelectedValue = dr["title"].ToString();
            FirstName_TextBox.Text = dr["firstName"].ToString();
            LastName_TextBox.Text = dr["lastName"].ToString();
            Street_TextBox.Text = dr["street"].ToString();
            PostalCode_TextBox.Text = dr["postalCode"].ToString();
            City_TextBox.Text = dr["city"].ToString();
            DateTime bd = (DateTime)dr["birthdate"];
            BirthDate_TextBox.Text = bd.ToShortDateString();
            //FullName_Textblock.Text = ;
            Info_TextBlock.Text = dr["firstName"].ToString() + " " + dr["lastName"].ToString() + " has borrowed the following videos:";
            LoadVideos();
        }

        private void Title_ComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            List<string> data = new List<string>();
            data.Add("Mr");
            data.Add("Ms");
            data.Add("Mrs");

            // ... Get the ComboBox reference.
            var comboBox = sender as ComboBox;

             //... Assign the ItemsSource to the List.
            comboBox.ItemsSource = data;
            
        }

        private void Save_Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult res = MessageBox.Show("Do you want to save the changes?", 
                "Question", MessageBoxButton.YesNo, 
                MessageBoxImage.Question, MessageBoxResult.No);
            switch(res)
            {
                case MessageBoxResult.Yes:
                    SaveChanges();
                    break;
                case MessageBoxResult.No:
                    break;
            }
        }
        private void SaveChanges()
        {      
            string title = Title_ComboBox.SelectedValue.ToString();
            string firstName = FirstName_TextBox.Text;
            string lastName = LastName_TextBox.Text;
            string street = Street_TextBox.Text;
            int postalCode = Convert.ToInt32(PostalCode_TextBox.Text);
            string city = City_TextBox.Text;
            DateTime birthdate = DateTime.Parse(BirthDate_TextBox.Text);
            string sqlEx = "sp_UpdateCustomer";
            string id = ID_TextBlock.Text;

            connection.Open();
            SqlCommand command = new SqlCommand(sqlEx, connection);
            command.CommandType = CommandType.StoredProcedure;

            //SqlParameter imageParam = new SqlParameter
            //{
            //    ParameterName = "@customerImage",
            //    Value = picPath

            //};
            //command.Parameters.Add(imageParam);
            SqlParameter titleParam = new SqlParameter
            {
                ParameterName = "@title",
                Value = title
            };
            command.Parameters.Add(titleParam);

            SqlParameter idParam = new SqlParameter
            {
                ParameterName = "@p_customer_nr",
                Value = id
            };
            command.Parameters.Add(idParam);

            SqlParameter firstNameParam = new SqlParameter
            {
                ParameterName = "@firstName",
                Value = firstName
            };
            command.Parameters.Add(firstNameParam);

            SqlParameter lastNameParam = new SqlParameter
            {
                ParameterName = "@lastName",
                Value = lastName
            };
            command.Parameters.Add(lastNameParam);

            SqlParameter streetParam = new SqlParameter
            {
                ParameterName = "@street",
                Value = street
            };
            command.Parameters.Add(streetParam);

            SqlParameter postalCodeParam = new SqlParameter
            {
                ParameterName = "@postalCode",
                Value = postalCode
            };
            command.Parameters.Add(postalCodeParam);

            SqlParameter cityParam = new SqlParameter
            {
                ParameterName = "@city",
                Value = city
            };
            command.Parameters.Add(cityParam);

            SqlParameter birthdateParam = new SqlParameter
            {
                ParameterName = "@birthdate",
                Value = birthdate
            };
            command.Parameters.Add(birthdateParam);
            command.CommandText = @"Update T_Customers SET customerImage=";

            var result = command.ExecuteNonQuery();
            Message_TextBlock.Text = "Updated.";
        }
        private void Cancel_Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Delete_Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult res = MessageBox.Show("Do you want to delete this customer?",
               "Question", MessageBoxButton.YesNo,
               MessageBoxImage.Question, MessageBoxResult.No);
            switch (res)
            {
                case MessageBoxResult.Yes:
                    DeleteUser();
                    break;
                case MessageBoxResult.No:
                    break;
            }
        }

        private void DeleteUser()
        {            
            using(connection)
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.CommandText = "DELETE FROM T_Customers WHERE p_customer_nr = " + customerId;
                command.Connection = connection;
                command.ExecuteNonQuery();
            }
            ID_TextBlock.Foreground = Brushes.Gray;
            Title_ComboBox.IsEnabled = false;
            FirstName_TextBox.IsEnabled = false;
            LastName_TextBox.IsEnabled = false;
            Street_TextBox.IsEnabled = false;
            PostalCode_TextBox.IsEnabled = false;
            City_TextBox.IsEnabled = false;
            BirthDate_TextBox.IsEnabled = false;
            CustomerId_TextBlock.IsEnabled = false;
            Message_TextBlock.Text = "Customer deleted.";
            Title_ComboBox.IsEnabled = false;            
        }

        private void ChangeImg_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog ofdImage = new OpenFileDialog();
                ofdImage.Filter = "JPEG Files|*.jpg|Bitmap Files|*.bmp|Gif Files|*.gif";
                ofdImage.DefaultExt = "jpg";
                ofdImage.FilterIndex = 1;
                if (ofdImage.ShowDialog() == true)
                {
                    Stream s = ofdImage.OpenFile();
                    this.Customer_Image.Source = new BitmapImage(new Uri(ofdImage.FileName));
                    picPath = ofdImage.SafeFileName.ToString();
                    
                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Image_Loaded(object sender, RoutedEventArgs e)
        {
            //string imgLoc = "";
        }

        private void LoadVideos()
        {
            string sqlEx = "SELECT T_Videos.p_vid_nr, T_Videos.title, T_Customers_Videos.borrowDate, T_Customers_Videos.returnDate " +
                            "FROM T_Videos, T_Customers_Videos " +
                            "WHERE T_Customers_Videos.fp_vid_nr = T_Videos.p_vid_nr " +
                            "AND T_Customers_Videos.fp_customer_nr = " + customerId.ToString();
            videoTable = new DataTable();            
            sqlcmd = new SqlCommand(sqlEx, connection);
            adapter = new SqlDataAdapter(sqlcmd);
            connection.Open();
            adapter.Fill(videoTable);
            videoGrid.ItemsSource = videoTable.DefaultView;

            if (connection != null)
                connection.Close();
        }

        private void videoGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Return_button.IsEnabled = true;
            if (videoGrid.SelectedItem != null)
            {
                if(videoGrid.Items.Count != 0)
                {
                    string idString = ((DataRowView)(videoGrid.SelectedItem)).Row.ItemArray[0].ToString();
                    videoId = Convert.ToInt32(idString);
                }
            }
        }

        private void Borrow_button_Click(object sender, RoutedEventArgs e)
        {
            //string idString = ((DataRowView)(videoGrid.SelectedItem)).Row.ItemArray[0].ToString();
            //int videoId = Convert.ToInt32(idString);
            int borrowedVideos;
            string sqlQuery2 = "SELECT COUNT(fp_customer_nr) FROM T_Customers_Videos WHERE fp_customer_nr = " + customerId;

            connection.Open();
            if (videoGrid.SelectedItem != null)                                             //Sieht sinnlos aus, aber anders geht nicht.
            {
                SqlCommand sqlcmd2 = new SqlCommand(sqlQuery2, connection);
                Int32.TryParse(sqlcmd2.ExecuteScalar().ToString(), out borrowedVideos);
            }
            else
            {
                SqlCommand sqlcmd2 = new SqlCommand(sqlQuery2, connection);
                Int32.TryParse(sqlcmd2.ExecuteScalar().ToString(), out borrowedVideos);
            }
            connection.Close();
               

            if (borrowedVideos < 5)
            {
                SelectVideo_Window svw = new SelectVideo_Window(customerId);
                svw.Show();
            }
            else MessageBox.Show("Sorry, you cannot borrow more than five videos at the same time.");


            if (videoGrid.SelectedItem == null)
            {
                Return_button.IsEnabled = false;                
            }
        }

        private void Return_button_Click(object sender, RoutedEventArgs e)
        {
            if (videoGrid.SelectedItem == null)
            {
                Return_button.IsEnabled = false;
            }
            else
            {
                string sqlEx = "DELETE FROM T_Customers_Videos WHERE fp_customer_nr = " + customerId +
                    " AND fp_vid_nr = " + videoId;
                sqlcmd = new SqlCommand(sqlEx, connection);
                connection.Open();
                sqlcmd.ExecuteNonQuery();
                MessageBox.Show("Video returned.");
                connection.Close();
                Return_button.IsEnabled = false;
                UpdateAvailableAmount();
            }
        }

        public void UpdateAvailableAmount()
        {
            int totalAmount;
            int availableAmount;
            int borrowedAmount;
            string sqlQuery = "SELECT amountTotal FROM T_Videos WHERE p_vid_nr = " + videoId;
            string sqlQuery1 = "SELECT COUNT (*) FROM T_Customers_Videos WHERE fp_vid_nr = " + videoId;
            SqlCommand sqlcmd = new SqlCommand(sqlQuery, connection);
            SqlCommand sqlcmd1 = new SqlCommand(sqlQuery1, connection);
            connection.Open();
            Int32.TryParse(sqlcmd.ExecuteScalar().ToString(), out totalAmount);
            Int32.TryParse(sqlcmd1.ExecuteScalar().ToString(), out borrowedAmount);
            connection.Close();
            availableAmount = totalAmount - borrowedAmount;
            string sqlQuery2 = "UPDATE T_Videos SET amountAvailable = " + availableAmount +
                " WHERE p_vid_nr = " + videoId; ;
            SqlCommand sqlcmd2 = new SqlCommand(sqlQuery2, connection);
            connection.Open();
            sqlcmd2.ExecuteNonQuery();
            connection.Close();
        }

        private void Refresh_button_Click(object sender, RoutedEventArgs e)
        {
            LoadVideos();
        }
    }
}
