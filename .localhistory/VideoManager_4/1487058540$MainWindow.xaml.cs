﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace VideoManager_4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        SqlDataAdapter adapter;
        DataTable videoTable;
        DataTable customersTable;      
        public MainWindow()
        {
            InitializeComponent();  
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            CustomerInfo_Button.IsEnabled = false;
            VideoInfo_Button.IsEnabled = false;
            string sqlEx = "SELECT * FROM T_Videos";
            videoTable = new DataTable();
            SqlConnection connection = null;
            
                connection = new SqlConnection(connectionString);
                SqlCommand command = new SqlCommand(sqlEx, connection);
                adapter = new SqlDataAdapter(command);
                connection.Open();
                adapter.Fill(videoTable);
                videoGrid.ItemsSource = videoTable.DefaultView;   

                if (connection != null)
                    connection.Close();
            string selCustomers = "SELECT * FROM T_Customers";
            customersTable = new DataTable();
            connection = null;

            connection = new SqlConnection(connectionString);
            command = new SqlCommand(selCustomers, connection);
            adapter = new SqlDataAdapter(command);
            connection.Open();
            adapter.Fill(customersTable);
            customersGrid.ItemsSource = customersTable.DefaultView;

            if (connection != null)
                connection.Close();
        }

        private void findButton_Click(object sender, RoutedEventArgs e)
        {
            string sqlEx = "SELECT * FROM T_Videos WHERE title LIKE '%' + @title + '%'";
            string title = findTextBox.Text;
            videoTable = new DataTable();
            SqlConnection connection = null;

            connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sqlEx, connection);
            SqlParameter nameParam = new SqlParameter
            {
                ParameterName = "@title",
                Value = title
            };
            command.Parameters.Add(nameParam);
            adapter = new SqlDataAdapter(command);
            connection.Open();
            adapter.Fill(videoTable);
            videoGrid.ItemsSource = videoTable.DefaultView;

            if (connection != null)
                connection.Close();
        }

        //private void changeListButton_Click(object sender, RoutedEventArgs e)
        //{
        //    CustomersWindow cw = new CustomersWindow();
        //    cw.Show();
        //}

       

        private void videoGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            VideoInfo_Button.IsEnabled = true;
        }

        private void infoButton_Click(object sender, RoutedEventArgs e)
        {
            string idString = ((DataRowView)(customersGrid.SelectedItem)).Row.ItemArray[0].ToString();
            int id = Convert.ToInt32(idString);
            CustomerInfoWindow ciw = new CustomerInfoWindow(id);
            ciw.Show();
        }

        private void AddNew_Button_Click(object sender, RoutedEventArgs e)
        {
            AddNewUser_Window anrWindor = new AddNewUser_Window();
            anrWindor.Show();
        }

        private void customersGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CustomerInfo_Button.IsEnabled = true;
        }

        private void CustomerInfoButton_Click(object sender, RoutedEventArgs e)
        {
            string idString = ((DataRowView)(customersGrid.SelectedItem)).Row.ItemArray[0].ToString();
            int id = Convert.ToInt32(idString);
            CustomerInfoWindow ciw = new CustomerInfoWindow(id);
            ciw.Show();
        }

        private void VideoInfo_Button_Click(object sender, RoutedEventArgs e)
        {
            string idString = ((DataRowView)(videoGrid.SelectedItem)).Row.ItemArray[0].ToString();
            int id = Convert.ToInt32(idString);
            VideoInfo_Window viw = new VideoInfo_Window(id);
            viw.Show();
        }
    }
}
