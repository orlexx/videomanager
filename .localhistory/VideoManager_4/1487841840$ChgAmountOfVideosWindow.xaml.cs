﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace VideoManager_4
{
    /// <summary>
    /// Interaction logic for ChgAmountOfVideosWindow.xaml
    /// </summary>
    public partial class ChgAmountOfVideosWindow : Window
    {
        int originalAmount;
        int availableAmount;
        int delta;
        static string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        SqlConnection con = new SqlConnection(cs);
        string sqlQuery;
        string sqlQuery1;
        int id;

        public ChgAmountOfVideosWindow(int id)
        {
            InitializeComponent();
            this.id = id;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            sqlQuery = "SELECT amountTotal FROM T_Videos WHERE " +
                "p_vid_nr = " + id;
            sqlQuery1 = "SELECT amountAvailable FROM T_Videos WHERE " +
                "p_vid_nr = " + id;
            SqlCommand sqlcmd = new SqlCommand(sqlQuery, con);
            SqlCommand sqlcmd1 = new SqlCommand(sqlQuery1, con);
            con.Open();
            Int32.TryParse(sqlcmd.ExecuteScalar().ToString(), out originalAmount);
            Int32.TryParse(sqlcmd1.ExecuteScalar().ToString(), out availableAmount);
            con.Close();
        }

        private void IncreaseButton_Click(object sender, RoutedEventArgs e)
        {
            if(int.TryParse(AmountTextBox.Text, out delta))
            {
                if (delta > 0)
                {
                    int newAmount = originalAmount + delta;
                    int newAvailableAmount = availableAmount + delta;
                    sqlQuery = "UPDATE T_Videos SET amountTotal = " + newAmount +
                        "WHERE p_vid_nr = " + id;
                    sqlQuery1 = "UPDATE T_Videos SET amountAvailable = " + newAvailableAmount +
                        "WHERE p_vid_nr = " + id;
                    con.Open();
                    SqlCommand sqlcmd = new SqlCommand(sqlQuery, con);
                    SqlCommand sqlcmd1 = new SqlCommand(sqlQuery1, con);
                    sqlcmd.ExecuteNonQuery();
                    sqlcmd1.ExecuteNonQuery();
                    con.Close();
                        Close();
                }
            }         
        }

        private void DecreaseButton_Click(object sender, RoutedEventArgs e)
        {
            if (int.TryParse(AmountTextBox.Text, out delta))
            {
                if (delta > 0 && originalAmount >= delta)
                {
                    int newAmount = originalAmount - delta;
                    sqlQuery = "UPDATE T_Videos SET amountTotal = " + newAmount +
                        "WHERE p_vid_nr = " + id;
                    con.Open();
                    SqlCommand sqlcmd = new SqlCommand(sqlQuery, con);
                    sqlcmd.ExecuteNonQuery();
                    con.Close();
                    Close();
                }
            }
                
        }

        
    }
}
