﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace VideoManager_4
{
    /// <summary>
    /// Interaction logic for VideoInfo_Window.xaml
    /// </summary>
    public partial class VideoInfo_Window : Window
    {
        static string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        SqlConnection con = new SqlConnection(cs);
        string sqlQuery;
        DataSet ds;
        public int id { get; set; }
        public VideoInfo_Window(int id)
        {
            InitializeComponent();
            this.id = id;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {            
            ID_TextBlock.Text = id.ToString();
            
            sqlQuery = "SELECT * FROM T_Videos WHERE p_vid_nr = " + id;
            SqlDataAdapter da = new SqlDataAdapter(sqlQuery, con);
            ds = new DataSet();
            da.Fill(ds, "Videos");
            DataRow dr = ds.Tables["Videos"].Rows[0];
            Title_TextBox.Text = dr["title"].ToString();
            Storage_ComboBox.SelectedValue = dr["storage"].ToString();
            Category_ComboBox.SelectedValue = dr["category"].ToString();
            //AgeRestriction_TextBox.Text = dr["ageRestriction"].ToString();

            int ageRestriction;

            ReleaseYear_TextBox.Text = dr["releaseYear"].ToString();
            Description_TextBox.Text = dr["videoDescription"].ToString();
            AmountTotal_TextBlock.Text = dr["amountTotal"].ToString();
            AmountAvailable_TextBlock.Text = dr["amountAvailable"].ToString();
        }
        public VideoInfo_Window()
        {
            InitializeComponent();
        }
        
        private void Category_ComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            List<string> data = new List<string>();
            data.Add("Cartoon");
            data.Add("Drama");
            data.Add("Action");
            data.Add("Adventure");
            data.Add("Detective fiction");
            data.Add("Documentary");
            data.Add("Fantasy");
            data.Add("Science-fiction");
            data.Add("Horror");
            data.Add("Thriller");
            data.Add("Comedy");
            data.Add("Educational");

            // ... Get the ComboBox reference.
            var comboBox = sender as ComboBox;

            //... Assign the ItemsSource to the List.
            comboBox.ItemsSource = data;
        }

        private void Storage_ComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            List<string> data = new List<string>();
            data.Add("VHS");
            data.Add("DVD");
            data.Add("bluray");

            // ... Get the ComboBox reference.
            var comboBox = sender as ComboBox;

            //... Assign the ItemsSource to the List.
            comboBox.ItemsSource = data;
        }

        private void Save_Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult res = MessageBox.Show("Do you want to save the changes?",
                "Question", MessageBoxButton.YesNo,
                MessageBoxImage.Question, MessageBoxResult.No);
            switch (res)
            {
                case MessageBoxResult.Yes:
                    SaveChanges();
                    break;
                case MessageBoxResult.No:
                    break;
            }
        }

        private void Cancel_Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Delete_Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult res = MessageBox.Show("Do you want to delete this video?",
               "Question", MessageBoxButton.YesNo,
               MessageBoxImage.Question, MessageBoxResult.No);
            switch (res)
            {
                case MessageBoxResult.Yes:
                    DeleteVideo();
                    break;
                case MessageBoxResult.No:
                    break;
            }
        }
        private void DeleteVideo()
        {
            using (con)
            {
                con.Open();
                SqlCommand command = new SqlCommand();
                command.CommandText = "DELETE FROM T_Videos WHERE p_vid_nr = " + id;
                command.Connection = con;
                command.ExecuteNonQuery();
            }
            ID_TextBlock.Foreground = Brushes.Gray;
            Title_TextBox.IsEnabled = false;
            Storage_ComboBox.IsEnabled = false;
            Category_ComboBox.IsEnabled = false;
            AgeRestriction_TextBox.IsEnabled = false;
            ReleaseYear_TextBox.IsEnabled = false;
            Description_TextBox.IsEnabled = false;
            Message_TextBlock.Text = "This video has been removed.";
        }
        private void SaveChanges()
        {
            string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            SqlConnection con = new SqlConnection(cs);

            string title = Title_TextBox.Text;
            string storage = Storage_ComboBox.SelectedValue.ToString();
            string category = Category_ComboBox.SelectedValue.ToString();
            string ageRestriction = AgeRestriction_TextBox.Text;
            string releaseYear = ReleaseYear_TextBox.Text;
            string videoDescription = Description_TextBox.Text;
            
            string sqlEx = "sp_UpdateVideo";
            string id = ID_TextBlock.Text;

            con.Open();
            SqlCommand command = new SqlCommand(sqlEx, con);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter titleParam = new SqlParameter
            {
                ParameterName = "@title",
                Value = title
            };
            command.Parameters.Add(titleParam);

            SqlParameter idParam = new SqlParameter
            {
                ParameterName = "@p_vid_nr",
                Value = id
            };
            command.Parameters.Add(idParam);

            SqlParameter storageParam = new SqlParameter
            {
                ParameterName = "@storage",
                Value = storage
            };
            command.Parameters.Add(storageParam);

            SqlParameter categoryParam = new SqlParameter
            {
                ParameterName = "@category",
                Value = category
            };
            command.Parameters.Add(categoryParam);

            SqlParameter ageRestrictionParam = new SqlParameter
            {
                ParameterName = "@ageRestriction",
                Value = ageRestriction
            };
            command.Parameters.Add(ageRestrictionParam);

            SqlParameter releaseYearParam = new SqlParameter
            {
                ParameterName = "@releaseYear",
                Value = releaseYear
            };
            command.Parameters.Add(releaseYearParam);

            SqlParameter descriptParam = new SqlParameter
            {
                ParameterName = "@videoDescription",
                Value = videoDescription
            };
            command.Parameters.Add(descriptParam);
            
            var result = command.ExecuteNonQuery();
            Message_TextBlock.Text = "Updated.";
        }

        private void ChgAmount_Button_Click(object sender, RoutedEventArgs e)
        {
            ChgAmountOfVideosWindow caw = new ChgAmountOfVideosWindow(Convert.ToInt32(ID_TextBlock.Text));
            caw.Show();

        }
    }
}
